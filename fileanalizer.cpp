#include "fileanalizer.h"

#include <map>
#include <iostream>
#include <fstream>
#include <cctype>

namespace analizer {

using std::cout;
using std::endl;
using std::cerr;

using std::string;

FileAnalizer::FileAnalizer() : sentences(0)
{
    using std::map;
    try{
        words_counter = new map<string, int>;
    } catch (std::bad_alloc& er){
        cerr << "Error! " << er.what() << endl;
    }
}

FileAnalizer::~FileAnalizer()
{
    delete words_counter;
    file.close();
}


void FileAnalizer::Analize()
{
    char symbol = '\0';
    // Remember a previous symbol to check for "..."
    char prev_symbol = '\0';
    // Compose a words from letter
    string word;

    while (file.good()){
        symbol = file.get();
        if ((symbol == '.') || (symbol == '!') || (symbol == '?')){
            // In case "..." will be only one increment
            if (prev_symbol == '.'){
                continue;
            } else
                ++sentences;
        }
        prev_symbol = symbol;
        // Make words unique
        if (isalpha(symbol))
            symbol = tolower(symbol);
        // Add detached words from separated signs
        if (isspace(symbol) || (symbol == '.') ||
                (symbol == ';') || (symbol == ',') || (symbol == '-') ||
                (symbol == '!') || (symbol == '?')){
            AddWord(word);
            word.clear();
        } else
            // Make a word to containe it further
            word += symbol;
    }
}

bool FileAnalizer::SetName(string path)
{
  	#ifdef __linux__
  	int pos = path.find_last_of("/");
  	#elif _WIN32
  	int pos = path.find_last_of("\\");
    #endif
    name = path.substr(pos+1);
    // Verify if a name of file has an extension
    if (name.find(".") != std::string::npos){
        return true;
    } else
        return false;
}

void FileAnalizer::AddWord(const string& word)
{
    // If a word repeated - increment it
    // else create a pair
    if (!words_counter->empty()){
        it = words_counter->find(word);
        if (it != words_counter->end()){
            it->second++;
            return;
        }
    }
    if (!word.empty())
        words_counter->insert(std::make_pair(word, 1));
}

bool FileAnalizer::SetPath(const char *pathToFile)
{
    file.open(pathToFile);
    // Verify a path
    if (!SetName(pathToFile)){
        cerr << "Bad path!" << endl;
        return false;
    }
    if (file.is_open()){
        cout << "File " << pathToFile <<
                " has opened." << std::endl;
        // Count a sentences and fill up a container with unique words
        Analize();
        return true;
    } else {
        cout << "Can't open " << pathToFile <<
                "." << std::endl;
        return false;
    }
}

const char *FileAnalizer::GetName() const
{
    return name.c_str();
}

int FileAnalizer::GetNumSentences() const
{
    return sentences;
}

int FileAnalizer::GetNumWords() const
{
    return words_counter->size();
}

std::ostream &operator<<(std::ostream &out, const FileAnalizer &object)
{
    out << "File: ";
    out << object.GetName() << "\n";
    out << "Total number of sentences: " << object.GetNumSentences() << "\n";
    out << "Total number of words: " << object.GetNumWords() << "\n";
    return out;
}


}
