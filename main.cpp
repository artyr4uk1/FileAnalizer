#include "fileanalizer.h"
#include <iostream>

int main(int argc, char *argv[])
{
    using analizer::FileAnalizer;

    FileAnalizer file_info;
    if (file_info.SetPath(argv[1])){
        std::cout << file_info;
    }
    return 0;
}
