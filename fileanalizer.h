#ifndef FILEANALIZER_H
#define FILEANALIZER_H

#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <map>

namespace analizer {

// Provides counting of sentences and words of text
class FileAnalizer{
private:
    std::ifstream file;
    std::map<std::string,int>* words_counter;
    std::map<std::string,int>::iterator it;

    int sentences;
    std::string name;

    void Analize();
    bool SetName(std::string path);
    // Store words in container
    void AddWord(const std::string &word);

public:
    FileAnalizer();
    ~FileAnalizer();

    bool SetPath(const char* pathToFile);
    const char *GetName() const;
    int GetNumSentences() const;
    int GetNumWords() const;

    // Show object info
    friend std::ostream &operator<<(std::ostream & out, const FileAnalizer& object);
};

}

#endif // FILEANALIZER_H
