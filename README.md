App accept pathToFile as command line argument so it can compile/link and run it from the command line as follows (example):
myfileanalyzer_demo somefile.txt
App print result to the console as follows (example):
File: somefile.txt
Total number of sentences: 7
Total number of words: 27
App also compilable and runnable on Linux OS and Windows OS(make -f Makefile).
